package pl.filmweb.rss;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.*;
import java.util.Collection;

public class NewsFeed {


    @Getter
    @Setter
    private boolean apiStatus = true;

    @Getter
    @Setter
    private HttpURLConnection request;

    @Getter
    @Setter
    private String restUrl;

    public NewsFeed(String restUrl) {

        URL url;

        try {
            url = new URL(restUrl);
            setRequest((HttpURLConnection) url.openConnection());
            request.setRequestProperty("Accept", "application/json");
            request.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            request.connect();
            int code = request.getResponseCode();
            if (code != 200 && code != 201) {
                setApiStatus(false);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public boolean isServerUp() {
        boolean status = true;
        try {
            InetSocketAddress inetSocketAddress = new InetSocketAddress("217.182.75.190", 8080);
            Socket ss = new Socket();
            ss.connect(inetSocketAddress, 30);
            System.out.println("connected");
            ss.close();
        } catch(Exception e) {
            System.out.println(e);

            status = false;
        }
        return status;
    }

    private String getJSON() throws IOException {

        String line;
        BufferedReader bufferedReader;
        StringBuilder stringBuilder;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(request.getInputStream()));
            stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) stringBuilder.append(line+"\n");
            bufferedReader.close();
        } finally {
            request.disconnect();
        }

        return stringBuilder.toString();
    }

    public Collection<News> getNewsFeed() throws IOException {

        String json = getJSON();
        Type collectionType = new TypeToken<Collection<News>>(){}.getType();
        Collection<News> enums = new Gson().fromJson(json, collectionType);
        return enums;

    }
}
