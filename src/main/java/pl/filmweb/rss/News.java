package pl.filmweb.rss;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class News {

    static AtomicInteger nextId = new AtomicInteger();

    @Getter
    @Setter
    private int id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private Date publicationDate;

    @Getter
    @Setter
    private String author;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private String image;

    public News(String title, String description, Date publicationDate, String author, String url, String image) {
        this.id = nextId.incrementAndGet();
        this.title = title;
        this.description = description;
        this.publicationDate = publicationDate;
        this.author = author;
        this.url = url;
        this.image = image;
    }

    String getTitle()
    {
        return this.title;
    }
    String getDescription()
    {
        return this.description;
    }
    Date getPublicationDate()
    {
        return this.publicationDate;
    }
    String getUrl()
    {
        return this.url;
    }
    String getAuthor() {return this.author;}
    String getImage() {return this.image;}
}
