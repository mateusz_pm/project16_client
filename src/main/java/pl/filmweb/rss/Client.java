package pl.filmweb.rss;

import org.jsoup.Jsoup;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Collection;

public class Client extends JFrame {
    //private JPanel mainPanel;
    private String[] tytuly = new String[20];
    private String[] opisy = new String[20];
    private String[] daty = new String[20];
    private String[] linki = new String[20];
    private String[] linkiObrazow = new String[20];
    private String[] autorzy = new String[20];
    int indeks = 0;
    int indeksGUI=1;
    String adresDoPdf;
    String adresSerwera;
    JLabel label = new JLabel();
    JTextPane pane7 = new JTextPane();
    JLabel label2=new JLabel();

    public Client() {

    //Do usuniecia, przykladowi autorzy i przykladowe linki obrazow

        //autorzy[0]="autor1";
        //autorzy[1]="autor2";

        //linkiObrazow[0]="http://1.fwcdn.pl/an/np/49468/2017/12633_1.7.jpg";
        //linkiObrazow[1]="http://1.fwcdn.pl/an/np/1985708/2017/12628_1.7.jpg";

        pane7.setBounds(50,600,580,25);
        pane7.setText("Miejsce na komunikaty systemu");
        add(pane7);

        /* Pobieranie JSON'a ze wskazanego adresu */
        NewsFeed nf = new NewsFeed("http://217.182.75.190:8080/api/news");

        if(!nf.isServerUp()) {
            System.out.println("Wystąpił błąd podczas łączenia, serwer offline!");
            pane7.setText("Wystąpił błąd podczas łączenia, serwer offline!");
            return;
        }

        // Błąd podczas pobierania danych : brak treści w kanale RSS

        if(!nf.isApiStatus()) {
            System.out.println("Wystąpił błąd podczas pobierania danych, brak treści w kanale RSS!");
            pane7.setText("Wystąpił błąd podczas pobierania danych, brak treści w kanale RSS!");
            return;
        }

        int i = 0;
        Collection<News> news;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            news = nf.getNewsFeed();
            for (News n : news) {
                if(i < 20) {
                    this.tytuly[i] = htmlToString(n.getTitle());
                    this.opisy[i] = htmlToString(n.getDescription());
                    this.daty[i] = formatter.format(n.getPublicationDate());
                    this.linki[i] = n.getUrl();
                    this.autorzy[i] = n.getAuthor();
                    this.linkiObrazow[i] = n.getImage();
                    i++;
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        indeks=0;

        setSize(1250,700); //650x700
        setLocation(50,50);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        /*
        final JTextPane pane5 = new JTextPane();
        pane5.setBounds(150,500,100,25);
        pane5.setText("NEWS NR: "+indeksGUI);
        add(pane5);
        */

        JButton button1 = new JButton("dalej");
        button1.setBounds(450,500,75,25);
        add(button1);

        /*
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wypisz(++indeks);
                super.label
            }
        });
        */

        JButton button2 = new JButton("wstecz");
        button2.setBounds(50,500,75,25);
        add(button2);

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                --indeksGUI;
                pane7.setText("Miejsce na komunikaty systemu");
                wypisz(--indeks);
            }
        });

        JTextPane pane1 = new JTextPane();
        pane1.setBounds(10,10,590,50);
        pane1.setText(tytuly[0]);
        add(pane1);

        JTextPane pane2 = new JTextPane();
        pane2.setBounds(10,175,590,250);
        pane2.setText(opisy[0]);
        add(pane2);

        JTextPane pane3 = new JTextPane();
        pane3.setBounds(10,75,590,25);
        pane3.setText(daty[0]);
        add(pane3);

        //JLabel label2 = new JLabel();
        label2.setBounds(10,125,590,25);
        label2.setText(linki[0]);
        add(label2);

        label2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                java.awt.Desktop desktop = java.awt.Desktop.getDesktop ();
                try {
                    desktop.browse(new java.net.URI(linki[indeks]));
                }
                catch (Exception e2) {
                    pane7.setText("Blad z przejsciem na pulpit!");
                }

            }
        });

        JTextPane pane5 = new JTextPane();
        pane5.setBounds(10,450,590,25);
        pane5.setText(autorzy[0]);
        add(pane5);

        JTextPane pane6 = new JTextPane();
        pane6.setBounds(250,500,100,25);
        pane6.setText("NEWS "+indeksGUI+"/20");
        add(pane6);

        //JTextPane pane7 = new JTextPane();


        JButton button4 =new JButton("stworz pdf");
        button4.setBounds(325,550,200,25);
        add(button4);

/*

        final JTextArea area1 = new JTextArea("adres internetowy");
        area1.setBounds(50,550,200,25);
        add(area1);

*/

        final JTextArea area2 = new JTextArea(".\\NEWSY FILMWEB.pdf");
        area2.setBounds(50,550,200,25);
        add(area2);

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adresDoPdf = area2.getText();
                System.out.println(adresDoPdf);
                PDFGen gen1 = new PDFGen();
                gen1.createPDF(getTytuly(), getOpisy(), getDaty(), getLinki(), getLinkiObrazow(), adresDoPdf);
            }
        });

        try {
            URL url = new URL(linkiObrazow[0]);
            //BufferedImage image = ImageIO.read(url);
            Image image=ImageIO.read(url);
            //JLabel label = new JLabel(new ImageIcon(image));
            //label.setIcon(new ImageIcon(image));
            /*
            label.setBounds(600,50,600,400);
            Image scaled=image.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
*/
            label.setBounds(600,50,616,193);
            Image scaled=image.getScaledInstance(616,193, Image.SCALE_SMOOTH);

            label.setIcon(new ImageIcon(scaled));
            add(label,BorderLayout.CENTER);
        }
        catch(Exception e)
        {
            System.out.println("BLAD!");
        }


        JButton button3 = new JButton("pobierz dane");
        button3.setBounds(325,550,200,25);
        add(button3);


/*
        //PRZYCISK DO POBIERANIA DANYCH!!

        /*

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wypisz(0);
                adresSerwera = area1.getText();
                System.out.println(adresSerwera);
                indeks=0;

            }
        });

*/

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ++indeksGUI;
                pane7.setText("Miejsce na komunikaty systemu");
                wypisz(++indeks);
            }
        });

        //pack();
        setLayout(null);
        setVisible(true);

}
public void wypisz(int indeks)
{
    JTextPane pane1 = new JTextPane();
    pane1.setBounds(10,10,590,50);

    JTextPane pane2 = new JTextPane();
    pane2.setBounds(10,175,590,250);

    JTextPane pane3 = new JTextPane();
    pane3.setBounds(10,75,590,25);

    //JTextPane pane4 = new JTextPane();
    //pane4.setBounds(10,125,590,25);

    JTextPane pane5 = new JTextPane();
    pane5.setBounds(10,450,590,25);

    JTextPane pane6 = new JTextPane();
    pane6.setBounds(250,500,100,25);

    try {

        URL url = new URL(linkiObrazow[indeks]);
        //BufferedImage image = ImageIO.read(url);
        Image image=ImageIO.read(url);
        //JLabel label = new JLabel(new ImageIcon(image));
        //label.setIcon(new ImageIcon(image));
        /*
        label.setBounds(600,50,600,400);
        Image scaled=image.getScaledInstance(600, 400, Image.SCALE_SMOOTH);
        */

        label.setBounds(600,50,616,193);
        Image scaled=image.getScaledInstance(616, 193, Image.SCALE_SMOOTH);

        label.setIcon(new ImageIcon(scaled));
        add(label,BorderLayout.CENTER);

    }
    catch(Exception e)
    {
        System.out.println("BLAD!");
    }


try {
    pane1.setText(tytuly[indeks]);
    pane2.setText(opisy[indeks]);
    pane3.setText(daty[indeks]);
    //pane4.setText(linki[indeks]);
    pane5.setText(autorzy[indeks]);
    pane6.setText("NEWS "+indeksGUI+"/20");
    label2.setText("");
    label2.setText(linki[indeks]);
}
catch(java.lang.ArrayIndexOutOfBoundsException e) {
    System.out.println("Indeks poza granicami! "+indeks);
    pane7.setText("Indeks poza granicami");
    if(indeks<0)
    {
        this.indeks=0;
        this.indeksGUI=1;
    }
    else if(indeks>(tytuly.length-1))
    {
        this.indeks = tytuly.length-1;
        this.indeksGUI=tytuly.length;
    }
}


    add(pane1);
    add(pane2);
    add(pane3);
    //add(pane4);
    add(pane5);
    add(pane6);
}
String[] getTytuly()
{
    return this.tytuly;
}
    String[] getOpisy()
    {
        return this.opisy;
    }
    String[] getDaty()
    {
        return this.daty;
    }
    String[] getLinki()
    {
        return this.linki;
    }
    String[] getLinkiObrazow()
    {
        return this.linkiObrazow;
    }



    private String htmlToString(String html) {
        return Jsoup.parse(html).text();
    }


}
