package pl.filmweb.rss;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;


public class PDFGen {

    void createPDF(String[] tytuly,String[] opisy,String[] daty,String[] linki,String[] linkiObrazow,String adresDoPdf) {
        try {
            String FILE1 = ".\\output.pdf"; //wpisz gdzie ma sie pojawic pdf i jaka ma byc jego nazwa
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(adresDoPdf));
            document.open();
            int i;
            //int liczbaNewsow=tytuly.length;
            int liczbaNewsow=19;
            for (i = 0; i < liczbaNewsow; i++) {
                document.add(new Paragraph(tytuly[i]));
                document.add(new Paragraph(opisy[i]));
                document.add(new Paragraph(daty[i]));
                document.add(new Paragraph(linki[i]));

                /*
                String imageUrl = linkiObrazow[i];
                Image image = Image.getInstance(imageUrl);
                //java.awt.Image image=ImageIO.read(imageUrl);
                //java.awt.Image scaled=image.getScaledInstance(616,193, java.awt.Image.SCALE_SMOOTH);
                document.add(image);
                */

                String imageUrl = linkiObrazow[i];
                //URL url = new URL(linkiObrazow[i]);
                //Image image = Image.getInstance(url);
                //Image image=Image.getInstance(imageUrl);
                /*
                Image image=Image.getInstance(new URL(imageUrl));

                image.scaleAbsolute(308,97);
                document.add(image);
*/
                document.add(new Paragraph("-----"));
            }
            document.close();
        }  catch (java.io.FileNotFoundException e1) {
            System.out.println("Nie znaleziono pliku!");
        }
        catch (com.itextpdf.text.DocumentException e2) {
            System.out.println("Blad z dokumentem!");
        }
        catch (java.io.IOException e3) {
            System.out.println("Blad z obrazkiem");

        }
    }
}
